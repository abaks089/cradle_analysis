import math

def summation_formula(data, index):
    value = data[index] - data[index + 1]
    return value ** 2


def summate(data):
    sum = 0
    for index in range(len(data) - 1):
        sum = sum + summation_formula(data, index)
    return sum

def rmssd(data):
    # We must convert the decimal list to a float list from Dynamo -> Python for calculations.
    floats = [float(i) for i in data]
    value_squared = (1 / (len(floats) - 1)) * summate(floats)
    return math.sqrt(value_squared)

