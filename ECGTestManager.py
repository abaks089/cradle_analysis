from ECGTest import ECGTest
import matplotlib.pyplot as plt
import boto3


def retrieve_ecg_table_items():
    table = boto3.resource('dynamodb').Table('ECGTestResults')
    response = table.scan()
    items = response['Items']
    return items


def retrieve_image_results_table_items():
    image_table = boto3.resource('dynamodb').Table('ImageResults')
    image_response = image_table.scan()
    image_items = image_response['Items']
    return image_items


class ECGTestManager:

    def __init__(self):
        self.dynamo_ecg_test_result_items = retrieve_ecg_table_items()
        self.dynamo_image_result_items = retrieve_image_results_table_items()

        self.ecg_post_baseline_map = self.__get_map_of_post_baseline()
        self.ecg_baseline_map = self.__get_map_of_baseline()

        self.image_result_map = self.__map_of_patients_to_image_results()

        self.ECGTestObjects_post_baselines = [ECGTest(item, self.image_result_map[item['PID']])
                                              for item in self.ecg_post_baseline_map.values()]

        # TODO: configure if we will evaluate
        # Reverse to put into it in ascending order
        # Since we reversed the sort to get the first exam, we're reversing it again to order it.
        self.ecg_baseline_test_list = reversed([item for item in self.ecg_baseline_map.values()])

        return

    def print_post_baselines(self):
        for item in self.ECGTestObjects_post_baselines:
            item.print()

    # TODO: configure if we will evaluate
    def print_raw_baselines(self):
        for baseline in self.ecg_baseline_test_list:
            for k, v in baseline.items():
                if (k == 'Signal' or k == 'BPM'):
                    print(k, '- ', [float(i) for i in v])
                    continue
                print(k,'- ', v)
            print()

    def plot_each_baseline_exam(self):
        for index, ecg_test in enumerate(self.ECGTestObjects_post_baselines):
            print("Test Item #", index)
            ecg_test.print()
            ecg_test.show_plot()
        return

    def plot_all_post_baseline_exams(self):
        image_description_list = []
        hrv_list = []
        for index, ecg_test in enumerate(self.ECGTestObjects_post_baselines):
            print("Test Item #", index)
            for k, v in ecg_test.segmented_data_calculation.items():
                image_description_list.append(v['image_type'].capitalize())
                hrv_list.append(v['hrv'])
        plt.plot(image_description_list, hrv_list, "ro")
        plt.xlabel('Image Exposure')
        plt.ylabel('Heart Rate Variability')
        plt.title('ALL PATIENTS @ ALL SEGEMENTS')
        plt.show()

    def __map_of_patients_to_image_results(self):
        map = {}
        for index, item in enumerate(sorted(self.dynamo_image_result_items, key=lambda k: k['TimeOfTest'])):
            map[item['PID']] = item['ImageDescription']
            # print(item['PID'])
        return map

    # THIS MAP ASSUMES THAT THE POST BASELINE WAS ALWAYS THE SECOND TEST.
    # WE MUST DO THIS BECAUSE THERE IS NO FLAG IN THE DATABASE TO DETERMINE TEST TYPE.
    # We will create a map that has a key for each PID,
    #  assuming the baseline exam is always first, it will always get overwritten by the post-baseline.
    def __get_map_of_post_baseline(self):
        map = {}
        for index, item in enumerate(sorted(self.dynamo_ecg_test_result_items, key=lambda k: k['TestDate'])):
            map[item["PID"]] = item
        return map

    def __get_map_of_baseline(self):
        map = {}
        for index, item in enumerate(reversed(sorted(self.dynamo_ecg_test_result_items, key=lambda k: k['TestDate']))):
            map[item["PID"]] = item
        return map

