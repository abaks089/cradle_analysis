import matplotlib.pyplot as plt
from hrv_calculator import rmssd


class ECGTest:

    def __init__(self, ecg_test_item, image_list):
        self.pid = ecg_test_item["PID"]
        self.date = ecg_test_item["TestDate"]
        self.hrv_of_exam = rmssd(ecg_test_item["BPM"])
        self.segmented_readings_list = self.__chunk_list_as_floats(ecg_test_item["BPM"], 12)
        self.baseline_bmp_list = list(self.segmented_readings_list[0])
        self.baseline_hrv = rmssd(
            self.baseline_bmp_list)  # this line does parse floats into floats during rmssd calculation
        self.image_list = image_list
        self.image_count = len(image_list)
        self.segmented_data_calculation = self.__get_calculated_data_map_from_segmentation()
        return

    def print(self):
        print("Patient ID: ", self.pid)
        print("\tDate: ", self.date)
        print("\tBPM of each lists (section): ", self.segmented_readings_list)
        print("\tImage List: ", self.image_list)
        print("\tImage Result Count: ", self.image_count)
        print("\tMap of Section Data: ", self.segmented_data_calculation)
        print("\tHRV of entire exam: ", self.hrv_of_exam)
        print("\tBaseline HRV (RMSSD): (first 30 seconds of exam)", self.baseline_hrv)
        print("\tHRV per segment(RMSSD): (post 30 seconds of exam)",
              self.list_of_calculations_excluding_baseline_for('hrv'))
        print("\tBaseline BPM List: ", self.baseline_bmp_list)
        return

    def show_plot(self):
        image_description_list = []
        hrv_list = self.list_of_calculations_for('hrv')
        for k, v in self.segmented_data_calculation.items():
            image_description_list.append(v['image_type'].capitalize() + "\n#" + str(k))

        plt.plot(image_description_list, hrv_list, linestyle='--', marker='o', color='b')
        plt.xlabel('Image Exposure')
        plt.ylabel('Heart Rate Variability')
        plt.title('Patient ID: ' + self.pid)
        plt.show()

    def list_of_calculations_for(self, map_key):
        return [val[map_key] for val in self.segmented_data_calculation.values()]

    def list_of_calculations_excluding_baseline_for(self, map_key):
        return [val[map_key] for val in self.segmented_data_calculation.values()][1:]

    def __get_calculated_data_map_from_segmentation(self):
        map = {}
        for i, section in enumerate(self.segmented_readings_list):
            description = self.__get_image_desc_for_section(i, self.image_list)
            map[i] = {
                "image_type": self.__get_image_emotion(description),
                "image_description": description,
                "hrv": rmssd(section),
                "hrv_difference_from_baseline": self.baseline_hrv - rmssd(section),
                "bpm_count": len(section)
            }
        return map

    # Divide BPM reading into almost equal or equal sections to calculate HRV
    def __chunk_list_as_floats(self, lst, n):
        return [[float(i) for i in lst[i::n]] for i in range(n if n < len(lst) else len(lst))]

    def __get_image_desc_for_section(self, section_index, image_list):
        # if the index of the section is odd, then that means that an image was shown
        # if the index of the section is even, then that means that a neutral image was shown.
        # map = {1:0, 3:1, 5:2, 7:3, 9:4, 11:5}
        # List Index is (section index -1 /2), must cast to an int from float to use as index
        list_index = int((section_index - 1) / 2)

        if section_index % 2 == 1:
            return image_list[list_index]
        else:
            return "Neutral Image"

    # last word of the image description
    def __get_image_emotion(self, image_description):
        first, *mid, last = image_description.split()
        # this variable is equal to the last word in the image_desc
        if last == "Image":
            return "neutral"
        return last.lower()
