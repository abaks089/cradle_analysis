from ECGTestManager import ECGTestManager

ecg_manager = ECGTestManager()

ecg_manager.print_raw_baselines()
ecg_manager.print_post_baselines()
# def run_on_cmd(cmd):
#     if cmd == 'plot':
#         ecg_manager.plot_each_baseline_exam()
#     elif cmd == "print-baselines":
#         print(
#             "These are the baseline exams that do not have any calculations, they have not been made into their own "
#             "class.")
#         ecg_manager.print_raw_baselines()
#     elif cmd == "print-post-baselines":
#         print(
#             "These are the post-baseline exams that contain all calculations, they have been made into their own class.")
#         ecg_manager.print_post_baselines()
#     elif cmd == 'plot-all':
#         ecg_manager.plot_all_post_baseline_exams()
#     else:
#         print("invalid entry")
#
# def run():
#     print('Commands: plot, plot-all, print-baselines, print-post-baselines')
#     cmd = 'temp'
#     while cmd != '-1':
#         cmd = input()
#         run_on_cmd(cmd)
#     print("!!!DONE!!!")
#
# run()